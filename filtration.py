import sys
import os
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib import colors as mcolors
import numpy as np

sys.path.insert(0, "./PitchSalami")
from PitchSalami import *

# the analysis is performed on the set of .mid files located on the directory ./pieces
# pitch-class histograms : l.285 - set variable save_pc_histo to True (l.269)
# n-pitch-class-sets histograms (n first, of length s) : l.278
# compare filtrations : l.288

input_midi_files_dir = "./pieces/"
input_midi_files_dir = "../../mozart-tonal-midi-C-test/"

reduced_piece_names = {
    "bwv254-debut.mid":"BWV 254",
    "Bach-chor-010.mid":"J.-S. Bach - chorale 10",
    "Bach-bwv367.mid":"J.-S. Bach - chorale BWV 367",
    "bwv264.mid":"J.-S. Bach - chorale BWV 264",
    "chor-001.mid":"BWV 253",
    "chor-002.mid":"BWV 254",
    "chor-003.mid":"BWV 255",
    "chor-004.mid":"J.-S. Bach - chorale 04",
    "chor-005.mid":"J.-S. Bach - chorale 05",
    "chor-006.mid":"J.-S. Bach - chorale 06",
    "chor-031.mid":"J.-S. Bach - chorale 31",
    "chor-032.mid":"J.-S. Bach - chorale 32",
    "chor-033.mid":"J.-S. Bach - chorale 33",
    "chor-034.mid":"J.-S. Bach - chorale 34",
    "Bach-fugue1.mid":"J.-S. Bach - Fugue 1",
    'Haydn-17-1-i.mid':'Haydn 17.1.i',
    'Haydn-17-6-i.mid':'Haydn 17.6.i',
    'Haydn-33-3-iii.mid':'Haydn 33.3.iii',
    'Haydn-64-4_debut.mid':'Haydn - string quartet 64-4',
    'Mozart-K80-1.mid':'W.A Mozart - string quartet K80-1',
    'beethoven_piano_sonata_49_2.mid':'L.V Beethoven - piano sonata 49',
    'Glass-mtamrph1.mid':'P. Glass - Methamorphosis 1',
    'Schoenberg-194.mid':'A. Schoenberg - 194',
    'Babbit-Semi-Simple_Variations.mid':'M. Babbit - Semi-simple variations',
    'aacluster.mid':"aacluster"
}

piece_names = {
"Bach-chor-010.mid":"J.-S. Bach - chorale 10",
"Bach-bwv367.mid":"J.-S. Bach - chorale BWV 367",
"BWV367_debut.mid":"J.-S. Bach - chorale BWV 367",
"bwv264.mid":"J.-S. Bach - chorale BWV 264",
"chor-001.mid":"J.-S. Bach - chorale 01",
"chor-002.mid":"J.-S. Bach - chorale 02",
"chor-003.mid":"J.-S. Bach - chorale 03",
"chor-004.mid":"J.-S. Bach - chorale 04",
"chor-005.mid":"J.-S. Bach - chorale 05",
"chor-006.mid":"J.-S. Bach - chorale 06",
"Bach-fugue1.mid":"J.-S. Bach - Fugue 1",
'Haydn-17-1-i.mid':'J. Haydn - string quartet 17-1-i',
'Haydn-64-4_debut.mid':'J. Haydn - string quartet Op.64-4',
'Mozart-K80-1.mid':'W.A Mozart - string quartet K80-1',
'beethoven_piano_sonata_49_2.mid':'L.V Beethoven - piano sonata Op.49-2',
'Glass-mtamrph1.mid':'P. Glass - Methamorphosis 1',
'Schoenberg-194.mid':'A. Schoenberg - 194',
'Babbit-Semi-Simple_Variations.mid':'M. Babbit - Semi-simple variations',
#'Arvo_Part_magnif.mid':'A. P\\"{a}rt - Magnificat',
'Arvo_Part_magnif.mid':'A. Pärt - Magnificat',
'aacluster.mid':"aacluster"
}


# Pas sûr que ça marche pour des comparaisons de filtration réduites à une unique taille de pcset -> appeler pour l'instant get_filtration_level_list() et get_occurences_above_ratio(r) sans paramètre de taille
def display_filtration_comparison(pcs_occs_list,title_list):
    pcs_occs_list=list(reversed(pcs_occs_list))
    title_list=list(reversed(title_list))
    number_of_pieces = len(pcs_occs_list)
    whole_pcss_list=[]
    whole_pcss_list.append([])
    recurrent_pcss_list=[]
    for i in range(len(pcs_occs_list)):
        print(title_list[i])
        pcs_occs=pcs_occs_list[i]
        level_list=pcs_occs.get_filtration_level_list()
        print("level_list : {}".format(level_list))
        for ratio in level_list:
            ratio_dict=pcs_occs.get_occurences_above_ratio(ratio)
            print("ratio:",ratio)
            pcset_list=[]
            for pcsid,occ in ratio_dict.items():
                pcss=get_pcset_from_ID(occ._pcs_id)
                pcset_list.append(pcss)
            print("pcset list :",pcset_list)
            
            for pcssw in whole_pcss_list:
                # print("pcset_list:",pcset_list)
                # print("pcssw:",pcssw)
                if pcsetset_t_equ(pcset_list,pcssw)!=-1:
                    # print("yes")
                    recurrent_pcss_list.append(pcset_list)
                    break
            else :
                whole_pcss_list.append(pcset_list)
    tpcss_colors = []
    j=0
    # print("whole_pcss_list:")
    for pcss in whole_pcss_list:
        #print(pcss)
        tpcss_color=None
        if tpcsetset_in_pcsetsetlist(pcss,recurrent_pcss_list):
            tpcss_color=colors[j]
            print("COLOR ",tpcss_color," - ",pcss)
            j=j+1
        else :
            tpcss_color="w"
        tpcss_colors.append(tpcss_color)
    #print(whole_pcss_list)
    tpcss_colors[0]='gray'
    # print("tpcss_colors:",tpcss_colors)
    # print("recurrent_pcss_list:")
    # for pcss in recurrent_pcss_list:
    #     print(pcss)
    #print(recurrent_pcss_list)
    number_of_pcss=len(whole_pcss_list)
            
    dataset=[]
    data_orders=[]
    for i in range(len(pcs_occs_list)):
        bar_data={}
        data_order=[]
        pcs_occs=pcs_occs_list[i]
        level_list=pcs_occs.get_filtration_level_list()
        bar_data[0]=1-level_list[0]
        data_order.append(0)
        for ratio_index in range(len(level_list)-1):
            ratio = level_list[ratio_index] # par ratio décroissant
            level_length=level_list[ratio_index]-level_list[ratio_index+1]
            ratio_dict=pcs_occs.get_occurences_above_ratio(ratio)
            pcset_list=[]
            pcset_list_index_in_whole_pcss_list=None
            for pcsid,occ in ratio_dict.items():
                pcss=get_pcset_from_ID(occ._pcs_id)
                pcset_list.append(pcss)
            for whole_index in range(len(whole_pcss_list)):
                pcss=whole_pcss_list[whole_index]
                if pcsetset_t_equ(pcset_list,pcss)!=-1:
                    pcset_list_index_in_whole_pcss_list=whole_index
                    break
            else :
                print("ERROR - tpcss not found in whole_pcss_list")
                exit()
            pcset_list_index_in_whole_pcss_list
            bar_data[pcset_list_index_in_whole_pcss_list]=level_length
            if ratio_index==len(level_list)-2:
                bar_data[pcset_list_index_in_whole_pcss_list]+=0.99-sum(list(bar_data.values()))
            data_order.append(pcset_list_index_in_whole_pcss_list)
        # completer dataset et data_orders avec les pcss manquants (duree=0) -> nécéssaire pour pyplot :(
        key_list=list(bar_data.keys())
        for j in range(len(whole_pcss_list)):
            if j not in key_list:
                bar_data[j]=0
                data_order.append(j)

        # print("BAR DATA : ",bar_data," s = ",sum(list(bar_data.values())))
        # print("data order : ",data_order)
        dataset.append(bar_data)
        data_orders.append(data_order)
    
    names = sorted(dataset[0].keys())
    values = np.array([[data[name] for name in order] for data,order in zip(dataset, data_orders)])
    lefts = np.insert(np.cumsum(values, axis=1),0,0, axis=1)[:, :-1]
    orders = np.array(data_orders)
    bottoms = np.arange(len(data_orders))
        
    fig_height = 0.4+number_of_pieces*0.4
    bar_height = 0.45
    
    plt.figure(figsize=(8, fig_height))# équivalent à l'étirement de la fenêtre
    
    for name, color in zip(names, tpcss_colors):
        #print(name," - ",color)
        idx = np.where(orders == name)
        #print("idx:",idx)
        value = values[idx]
        #print("value=",value)
        left = lefts[idx]
        plt.bar(x=left, height=bar_height, width=value, bottom=bottoms,color=color, orientation="horizontal", label=name, edgecolor="black",align="center")
        #break
    #plt.yticks(bottoms, ["data %d" % (t+1) for t in bottoms])
    plt.yticks(bottoms, title_list)
    plt.xticks(np.arange(0, 1, 0.1))
    
    #plt.legend(loc="best", bbox_to_anchor=(1.0, 1.00))
    plt.subplots_adjust(right=0.85)

    plt.gca().spines['left'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    if number_of_pieces==1:
        plt.gca().spines['bottom'].set_visible(False)
    
    myFormatter = ticker.FuncFormatter(lambda x,y: (1-x)) # pour transformer les labels de l'axe horizontal : x -> 1-x (cumulative duration based filtration)
    plt.gca().xaxis.set_major_formatter(myFormatter)

    plt.tight_layout() # pour adapter la figure aux labels (afin qu'ils ne dépassent pas)
    
    plt.show()
    #plt.savefig('test.pdf')


save_pc_histo = False

pcs_occs_list=[]
title_list=[]

for filename in os.listdir(input_midi_files_dir):
    if filename.endswith(".mid"):
        midi_file_name=filename
        midi_file_path=input_midi_files_dir+midi_file_name
        pitch_slicing = PitchSlicing(midi_file_path,60)     # to only take into account the n first slices
        #pitch_slicing = PitchSlicing(midi_file_path,None)
        #pitch_slicing.display()
        pcset_occurences=PCSetOccurrences(pitch_slicing=pitch_slicing)
        pcs_occs_list.append(pcset_occurences)
        if midi_file_name in reduced_piece_names:
            title_list.append(reduced_piece_names[midi_file_name])
        else :
            title_list.append(midi_file_name)
        print(midi_file_name)
        #display_most_frequent_pcss(piece_names[midi_file_name],pcset_occurences,10)
        piece_name_for_histo=None
        if midi_file_name in piece_names:
            piece_name_for_histo=piece_names[midi_file_name]
        else :
            piece_name_for_histo=midi_file_name
        if save_pc_histo:
            display_PC_histogram(piece_name_for_histo,pcset_occurences)
        
# Afficher sous formes de bares horizontales les filtrations des pièces analysées. Les portions de couleur similaire correspondent à des superlevel complexes T-equivalents
display_filtration_comparison(pcs_occs_list,title_list)
