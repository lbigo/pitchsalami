import os
from mido import MidiFile
import collections
from itertools import chain, combinations
from functools import reduce

import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib import colors as mcolors
import numpy as np


pc_names = {0:"C",1:"C#",2:"D",3:"Eb",4:"E",5:"F",6:"F#",7:"G",8:"G#",9:"A",10:"Bb",11:"B"}

def pset_to_pcset(p_set):
    pc_set = []
    for i in range(len(p_set)):
        pc_set.append(p_set[i]%12)
    return sorted(set(pc_set))

# returns -1 if not T-equivalent - returns t otherwise
def pcset_t_equ(pc_set1,pc_set2):
    if len(pc_set1) != len(pc_set2):
        return -1
    for t in range(12):
        t_pc_set1=list(map(lambda x: (x+t)%12,pc_set1))
        if all(elem in pc_set2  for elem in t_pc_set1):
            return t
    return -1

def pcs_in_pcss(pcs,pcss):
    for pcs2 in pcss:
        if len(pcs2)==len(pcs) and all(elem in pcs2  for elem in pcs):
            return True
    return False

def asc_equality(asc1,asc2):
    for pcs in asc1:
        if not pcs_in_pcss(pcs,asc2):
            return False
    for pcs in asc2:
        if not pcs_in_pcss(pcs,asc1):
            return False
    assert len(asc1)==len(asc2) , "WARNING : one abstract simplicial complexe must have a duplicate {}\n{}".format(asc1,asc2)
    return True

def set_equality(list1,list2):
    for i in list1:
        if i not in list2:
            return False
    for i in list2:
        if i not in list1:
            return False
    assert len(list1)==len(list2) , "WARNING : one set must have a duplicate {}\n{}".format(list1,list2)
    return True

def pcsetset_t_equ(pcsetset1,pcsetset2):
    if len(pcsetset1) != len(pcsetset2):
        return -1
    for t in range(12):
        #print("t=",t)
        for pcset1 in pcsetset1:
            t_pcset1 = list(map(lambda x: (x+t)%12,pcset1))
            #print("pcset1:",pcset1," -> ",t_pcset1)
            found_pcset2 = False
            if pcs_in_pcss(t_pcset1,pcsetset2):
                # passe ici si t(pcset1) est dans pcss2 : on passe directement au t(pcset1) suivant
                continue
            else :
                # passe ici si t(pcset1) n'est pas dans pcss2 : on peut donc directement passer au t suivant
                break
        else :
            # passe ici si le for pcset1 s'est terminé sans break
            return t
    return -1

def tpcsetset_in_pcsetsetlist(pcss,pcss_list):
    for pcss2 in pcss_list:
        if pcsetset_t_equ(pcss,pcss2)!=-1:
            return True
    return False

def get_pcset_ID(pcset):
    id = 0
    for i in range(len(pcset)):
        id = id+2**pcset[i]
    return id

def get_pcset_from_ID(id):
    assert id >=0 and id<4096
    pcset =[]
    tab =  [int(x) for x in bin(id)[2:]]
    tab = [0]*(12-len(tab))+tab
    for i in range(len(tab)):
        if tab[11-i]==1:
            pcset.append(i)
    return pcset

def get_pcset_str_from_ID(id):
    pcset=get_pcset_from_ID(id)
    pcset_names = list(map(lambda x: pc_names[x],pcset))
    s=str(pcset_names).replace("[","{").replace("]","}").replace("'","").replace(" ","")
    return s

def get_set_str_from_IDs(id_set):
    str=""
    for id in sorted(list(id_set)):
        str=str+get_pcset_str_from_ID(id)+" "
    return str[:-1] # remove last space

# returns all subsets except the empty set
def all_subsets(ss):
    it = chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))
    subsets = []
    for subset in it:
        if list(subset):
            subsets.append(list(subset))
    return subsets

def is_subset(set1,set2):
    return all(elem in set2 for elem in set1)

# returns a reduced representation of an abstract simplicial complex : faces are ommited for simplification
def asc_no_face_display(pcs_id_list):
    str=""
    pcs_set = list(map(lambda x:get_pcset_from_ID(x),pcs_id_list))
    pcs_set_no_faces = []
    for i in range(len(pcs_set)):
        is_face=False
        for j in range(len(pcs_set)):
            if i!=j:
                if is_subset(pcs_set[i],pcs_set[j]):
                    is_face=True
        if not is_face:
            pcs_set_no_faces.append(pcs_set[i])
    return pcs_set_no_faces

class PitchSlicing :
    
    def __init__(self,midi_file_path,ns=None,randomize=None):
        print("parsing "+midi_file_path)
        mid = MidiFile(midi_file_path)
        self._mid = mid
        self._file_name=midi_file_path.split('/')[-1]
        tick_list=[]

        # build list of MIDI ticks
        for i, track in enumerate(mid.tracks):
            absolute_tick = 0
            for msg in track:
                absolute_tick+=msg.time
                tick_list.append(absolute_tick)

        tick_list=sorted(list(set(tick_list)))

        track_dict_list = []
        track_index=0
        for it, track in enumerate(mid.tracks):
            track_index=+1
            slice_dict = {}
            for i in range(len(tick_list)-1):
                slice_dict[tick_list[i]]={"pitch_set":[],"duration":(tick_list[i+1]-tick_list[i])}
            message_tick = 0
            activated_pitchs = []
            tick_move=True
            last_tick = tick_list[len(tick_list)-1]
            previous_message_tick = 0
            for msg in track:
                if msg.time == 0:
                    tick_move=False
                else :
                    tick_move=True
                message_tick+=msg.time
                if message_tick != last_tick:   # do not create a new slice for the last tick of the track as the slice will not be closed
                    if tick_move :
                        for j in range(len(tick_list)-1):
                            if tick_list[j]<=message_tick and tick_list[j]>previous_message_tick :
                                slice_dict[tick_list[j]]["pitch_set"]=activated_pitchs.copy()
                    if msg.type=='note_on' and msg.velocity>0:
                        activated_pitchs.append(msg.note)
                    if msg.type=='note_off' or (msg.type=='note_on' and msg.velocity==0 and msg.note!=0):
                        if msg.note in activated_pitchs:
                            activated_pitchs.remove(msg.note)
                        else :
                            print("MIDI file error - tick "+str(message_tick)+" - tries to 'off' note "+str(msg.note)+" which was not previously 'on'")
                        
                    slice_dict[message_tick]["pitch_set"]=activated_pitchs.copy()
                    previous_message_tick=message_tick
            track_dict_list.append(slice_dict)
            

        # (vertical) merge of the track dict
        slice_dict = collections.OrderedDict()
        for i in range(len(tick_list)-1):
            slice_dict[tick_list[i]]={"pitch_set":[],"duration":(tick_list[i+1]-tick_list[i])}
            union_pitch_set = []
            for n in range(len(track_dict_list)):
                union_pitch_set.extend(track_dict_list[n][tick_list[i]]["pitch_set"])
            slice_dict[tick_list[i]]["pitch_set"]=union_pitch_set.copy()
                    
        # (horizontal) recursive merge of slices including identical pitch_sets

        def merge_first_two_same_pitch_set_slices(slice_dict):
            key_list = sorted(list(slice_dict.keys()))
            merged=False
            for i in range(len(key_list)-1):
                keyA = key_list[i]
                sliceA = slice_dict[keyA]
                keyB = key_list[i+1]
                sliceB = slice_dict[keyB]
                if set(sliceA["pitch_set"]) == set(sliceB["pitch_set"]):
                    sliceAB={"pitch_set":sliceA["pitch_set"].copy(),"duration":sliceA["duration"]+sliceB["duration"]}
                    del slice_dict[keyB]
                    slice_dict[keyA]=sliceAB
                    merge_first_two_same_pitch_set_slices(slice_dict)
                    break

        merge_first_two_same_pitch_set_slices(slice_dict)
        key_list = sorted(list(slice_dict.keys()))

        # removing pitch=0 in slices

        for i in range(len(key_list)):
            slice_dict[key_list[i]]["pitch_set"]=list(filter(lambda x: x!=0,slice_dict[key_list[i]]["pitch_set"]))

        # removing last empty slices

        last_pitch_set=slice_dict[key_list[-1]]['pitch_set']
        while not last_pitch_set:
            del slice_dict[key_list[-1]]
            del key_list[-1]
            last_pitch_set=slice_dict[key_list[-1]]['pitch_set']


        self._key_list = key_list
        self._slice_dict = slice_dict   # ordered set of slices as ordered dict of the form {key:offset,value:{pitchset:[..],duration:N}}
        
        print(self._file_name," reduced to ",len(self._key_list)," slices")
        # ne prendre que les ns premières slices
        if ns is not None :
            self._key_list=self._key_list[0:ns]
            while len(self._slice_dict)>ns:
                self._slice_dict.popitem()

    def get_slicing_duration(self):
        last_slice = list(self._slice_dict.items())[-1]
        return last_slice[0]+last_slice[1]["duration"]

    def get_key_list(self):
        return self._key_list
        
    def get_slicing(self):
        return self._slice_dict
        
    def get_pc_slicing(self):
        pc_slicing = {}
        for i in range(len(self._key_list)):
            pcset = pset_to_pcset(self._slice_dict[self._key_list[i]]["pitch_set"])
            pc_slice = {"pc_set":pcset,"duration":self._slice_dict[self._key_list[i]]["duration"]}
            pc_slicing[self._key_list[i]]=pc_slice
        return pc_slicing
    
    def get_covering_slice_key(self,tick):
        for i in range(len(self._key_list)):
            if tick < self._key_list[i]:
                return self._key_list[i-1]
        if tick<self.get_slicing_duration():
            return self._key_list[-1]
        print("tick was nout found : ",tick," - ",get_slicing_duration())
        exit()
        
    def transposed_pitch_slicing(self,n):
        transposed_slice_dict = {}
        for key in self._key_list:
            value_dic = self._slice_dict[key]
            t_value_dic = {"pitch_set":list(map(lambda x:x+n,value_dic["pitch_set"])),"duration":value_dic["duration"]}
            transposed_slice_dict[key]=t_value_dic
        return transposed_slice_dict
    
    def display(self,pc=None):
        print("\nPitch slicing of "+self._file_name+":")
        if pc is None :
            for i in range(len(self._key_list)):
                print ("key ",self._key_list[i]," : ",self._slice_dict[self._key_list[i]])
        else :  # TODO afficher que les PC (et non les pitchs)
            for i in range(len(self._key_list)):
                print ("key ",self._key_list[i]," : ",self._slice_dict[self._key_list[i]])

class PCSetOccurrence :
    def __init__(self,pcs_id):
        self._pcs_id = pcs_id
        self._slice_list = {}   # ordered set of slices as ordered dict of the form {key:offset,value:duration}
        
    def add_slice(self,offset,duration):
        assert offset not in self._slice_list
        self._slice_list[offset]=duration
        
    # needs window length in case the pcs set occurs at the last slice
    def get_cumul_duration(self,start,window_length):
        cumul_dur=0
        for offset,duration in self._slice_list.items():
            if offset+duration>=start and offset<start+window_length:
                cumul_dur+=duration+min(0,offset-start)-max(0,(offset+duration)-(start+window_length))
        return cumul_dur
    
    
class PCSetOccurrences :
    def __init__(self,pitch_slicing=None,audio_pc_slice_dict=None):
        # MIDI
        if pitch_slicing is not None :
            #self._pitch_slicing = pitch_slicing
            self._pcset_occurences = collections.OrderedDict() # ordered dict of pcset . {key:pcsID,value:PCSetOccurrence}
            self._slicing_duration = pitch_slicing.get_slicing_duration()
            self._file_name = pitch_slicing._file_name
            pc_slicing = pitch_slicing.get_pc_slicing()
            for offset, value in pc_slicing.items():
                pcs = value['pc_set']
                for subset in all_subsets(pcs):
                    subset_id=get_pcset_ID(subset)
                    if subset_id in self._pcset_occurences:
                        self._pcset_occurences[subset_id].add_slice(offset,value['duration'])
                    else :
                        pcset_occurrence=PCSetOccurrence(subset_id)
                        pcset_occurrence.add_slice(offset,value['duration'])
                        self._pcset_occurences[subset_id]=pcset_occurrence
        # AUDIO
        if audio_pc_slice_dict is not None :
            self._pcset_occurences = collections.OrderedDict() # ordered dict of pcset . {key:pcsID,value:PCSetOccurrence}
            slicing_duration = 0
            for key,slice in audio_pc_slice_dict.items():
                slicing_duration+=slice["duration"]
            self._slicing_duration=slicing_duration
            print("audio slicing duration : ",self._slicing_duration)
            for offset, value in audio_pc_slice_dict.items():
                pcs = value['pc_set']
                for subset in all_subsets(pcs):
                    subset_id=get_pcset_ID(subset)
                    if subset_id in self._pcset_occurences:
                        self._pcset_occurences[subset_id].add_slice(offset,value['duration'])
                    else :
                        pcset_occurrence=PCSetOccurrence(subset_id)
                        pcset_occurrence.add_slice(offset,value['duration'])
                        self._pcset_occurences[subset_id]=pcset_occurrence
            
    # returns a dict {pcs_id:pcs_occurrence,...}
    def get_occurences_above_ratio(self,ratio,pcset_size=None):
        #occurrences_sorted_by_cumul_duration = self.get_occurrences_sorted_by_cumul_duration()
        total_duration = self._slicing_duration
        filtered_pcset_occurences = collections.OrderedDict()
        pcset_occurences = self._pcset_occurences if pcset_size is None else self.get_occurences_size_n(pcset_size)
        for pcs_id, pcs_occurrence in pcset_occurences.items():
            if pcs_occurrence.get_cumul_duration(0,total_duration)/total_duration>=ratio:
                filtered_pcset_occurences[pcs_id]=pcs_occurrence
        return filtered_pcset_occurences
    
    # returns a dict. {pcs_id:occurence,...}
    def get_occurences_size_n(self,s):
        #occurrences_sorted_by_cumul_duration = self.get_occurrences_sorted_by_cumul_duration()
        filtered_pcset_occurences = collections.OrderedDict()
        for pcs_id, pcs_occurrence in self._pcset_occurences.items():
            if len(get_pcset_from_ID(pcs_id))==s:
                filtered_pcset_occurences[pcs_id]=pcs_occurrence
        return filtered_pcset_occurences # returns a dict {pcs_id:value,...}
            
    # returns a list of tuples (pcsID,PCSetOccurrence) sorted by cumulative duration. Only keeps pc-sets of size s (if s not None)
    def get_occurrences_sorted_by_cumul_duration(self,s=None):
        start = 0
        window_length = self._slicing_duration
        occurences_list = None
        if s is not None :
            occurences_list = list(self.get_occurences_size_n(s).items())
        else :
            occurences_list = list(self._pcset_occurences.items())
        sorted_occurences_list = sorted(occurences_list,key=lambda tup: tup[1].get_cumul_duration(start,window_length),reverse=True)
        # display
        for i in range(len(sorted_occurences_list)):
            id=sorted_occurences_list[i][0]
            cumul_duration = sorted_occurences_list[i][1].get_cumul_duration(start,window_length)
            print(id," - ",get_pcset_from_ID(id)," cumul dur : ",cumul_duration/self._slicing_duration," (",cumul_duration,")")
        return sorted_occurences_list
        
        
    # returns a list of 2-uples : [ (pcsID,PCSetOccurrence) ,...]
    def get_n_most_frequent_occurences(self,n,s=None):
        occurrences_sorted_by_cumul_duration = self.get_occurrences_sorted_by_cumul_duration(s)
        return occurrences_sorted_by_cumul_duration[0:n]
        
    # returns a dic. {0:n,1:n,...}
    def get_PC_histogram(self):
        pc_histo = {}
        occ_dic = self.get_occurences_size_n(1)
        for pc,name in pc_names.items():
            pc_id = get_pcset_ID([pc])
            if pc_id not in occ_dic:
                pc_histo[pc]=0
            else :
                pc_histo[pc]=occ_dic[pc_id].get_cumul_duration(0,self._slicing_duration)/self._slicing_duration
        return pc_histo
        
    # n = size of PCsets (not the asc dim !)
    def get_persistent_sets(self,n): # [(set_of_IDs:gap),(set_of_IDs:gap),...]
        pers_pcs_dict = []
        cumul_durations_list = []
        n_occs=self.get_occurences_size_n(n) # {pcs_id:pcs_occurence,...}
        for k,v in n_occs.items():  # fill cumul_durations_list with normalized durations (ie in [0..1])
            cumul_durations_list.append(v.get_cumul_duration(0,self._slicing_duration)/self._slicing_duration)
        cumul_durations_list=sorted(list(set(cumul_durations_list)),reverse=True)
        for i in range(len(cumul_durations_list)):
            value = cumul_durations_list[i]
            set_of_IDs = []
            for k,v in n_occs.items():
                norm_cumul_dur = v.get_cumul_duration(0,self._slicing_duration)/self._slicing_duration
                if norm_cumul_dur>=value:
                    set_of_IDs.append(k)
            if i==len(cumul_durations_list)-1:
                gap = value
            else :
                gap = value-cumul_durations_list[i+1]
            pers_pcs_dict.append((set(set_of_IDs),gap))
        for tuple in pers_pcs_dict:
            pcset_set = list(map(lambda x:get_pcset_from_ID(x),tuple[0]))
            # print("{} : gap={:.2f}".format(pcset_set,tuple[1]))
        return pers_pcs_dict
        
    def get_persistence_of_set(s):
        if len(s)==0 : # persistence of empty ASC = 0
            return 0
        for id in s :   # check that all pcsets in s are the same size
            assert len(get_pcset_from_ID(id)) == len(get_pcset_from_ID(s[0])), "all sets are not the same size : {} - {}".format(get_pcset_from_ID(id),get_pcset_from_ID(s[0]))
        pers_sets_dict=get_persistent_sets(len(get_pcset_from_ID(s[0])))
        for tuple in pers_pcs_dict:
            if set(s) == set(tuple[0]):
                return tuple[1]
        return 0
        
    def get_filtration_level_list(self,pcset_size=None):
        start = 0
        window_length = self._slicing_duration
        filtration_level_list=[]
        occurences_list = list(self._pcset_occurences.items()) if pcset_size is None else list(self.get_occurences_size_n(pcset_size).items())
        sorted_occurences_list = sorted(occurences_list,key=lambda tup: tup[1].get_cumul_duration(start,window_length),reverse=True)
        for i in range(len(sorted_occurences_list)):
            id=sorted_occurences_list[i][0]
            cumul_duration = sorted_occurences_list[i][1].get_cumul_duration(start,window_length)
            filtration_level_list.append(cumul_duration/self._slicing_duration)
            #print(id," - ",get_pcset_from_ID(id)," cumul dur : ",cumul_duration/self._slicing_duration," (",cumul_duration,")")
        return sorted(list(set(filtration_level_list)),reverse=True)
        
    # returns a list of dicts : [
    #                                {pcs_id_set:[149,321,...] , birth_level:xx , death_level:xx},
    #                                {pcs_id_set:[149,321,...] , birth_level:xx , death_level:xx},
    #                                {...},
    #                           ...]
    def get_abstract_simplicial_complexes_persistence(self,pcset_size=None):
        asc_persistences = []
        level_list=self.get_filtration_level_list(pcset_size)
        for i in range(len(level_list)):
            level=level_list[i]
            next_level = 0 if i==len(level_list)-1 else level_list[i+1]
            level_dict = self.get_occurences_above_ratio(level,pcset_size=pcset_size)
            pcsid_set = []
            for pcsid,occ in level_dict.items():
                pcsid_set.append(pcsid)
            birth_level=level
            death_level=next_level
            asc_persistences.append({"pcs_id_set":pcsid_set,"birth_level":birth_level,"death_level":death_level,"file_name":self._file_name})
        return asc_persistences

### PLOT AND STATS FUNCTIONS

latex_fonts = True

colors = ["tab:blue","tab:orange","tab:green","tab:red","tab:purple","tab:brown","tab:pink","tab:gray","tab:olive","tab:cyan",'b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
colors=colors+list(mcolors.CSS4_COLORS.values())

def display_most_frequent_pcss(title,pcs_occs,n,s = None):
    filtered_pcset_occurrences = pcs_occs.get_n_most_frequent_occurences(n,s)
    window_length=pcs_occs._slicing_duration
    labels = []
    values = []
    y_pos = np.arange(len(filtered_pcset_occurrences))
    for occurence in filtered_pcset_occurrences:   # pcs histogram
        labels.append(str(get_pcset_str_from_ID(occurence[0])))    # pcs histograms
        values.append(occurence[1].get_cumul_duration(0,window_length)/window_length)
    
    # latex fonts
    if latex_fonts :
        labels=list(map(lambda x: x.replace("#","$\sharp$").replace("{","\{").replace("}","\}"),labels))
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
    
    plt.figure(figsize=(3, 3))
    plt.bar(y_pos,values,align='center',width=0.5)
    plt.xticks(y_pos,labels,rotation=55,fontsize=7)
    #plt.ylabel('presence over the whole piece',fontsize=8)
    plt.ylabel('$\mathcal{D}_{S}(X)$',fontsize=8)
    plt.xlabel('$X$',fontsize=8)
    plt.ylim(top=1)
    plt.title(title,fontsize=8)
    plt.tight_layout()  # pour ne pas que les x-labels pivotés sortent de l'image
    plt.savefig('./plots/'+title+'.pdf')
    plt.clf()
    #plt.draw()

def display_corpus_most_frequent_pcss(title,pcs_occs_list,n,s = None):
    pcset_dur_lists = {}    # {pcset_id1 : [0.3, 0.2, 0.5], pcset_id2 : [0.1, 0.5], ...}
    for pcs_occs in pcs_occs_list:
        filtered_pcset_occurrences = pcs_occs.get_occurrences_sorted_by_cumul_duration(s)
        # filtered_pcset_occurrences = pcs_occs.get_n_most_frequent_occurences(n,s)
        # print("filtered_pcset_occurrences : {}".format(filtered_pcset_occurrences))
        for pcset_occurences in filtered_pcset_occurrences:
            pcset_id = pcset_occurences[0]
            norm_dur=pcset_occurences[1].get_cumul_duration(0,pcs_occs._slicing_duration)/pcs_occs._slicing_duration ## ???? en dev...
            if pcset_id not in pcset_dur_lists:
                pcset_dur_lists[pcset_id]=[norm_dur]
            else :
                pcset_dur_lists[pcset_id].append(norm_dur)
            # print("pcset id :{} ({}) norm_dur : {}".format(pcset_id,get_pcset_from_ID(pcset_id),norm_dur))
    # At this point, duration lists are not the same size. This is problematic to compute mean and std.
    # Therefore, lists must be "padded" with duration=0 to take into account pieces in which the pcset is absent
    for pcset_id, norm_dur_list in pcset_dur_lists.items():
        pcset_dur_lists[pcset_id]=norm_dur_list+[0]*(len(pcs_occs_list)-len(norm_dur_list))
    # print(pcset_dur_lists)
    
    pcset_durs_infos= {} # {pcsetid_1 : {mean:m,std:s}, pcsetid_2 : {mean:m,std:s}, ...}
    labels = []
    values = []
    stds = []
    for pcset_id, norm_dur_list in pcset_dur_lists.items():
        pcset_durs_infos[pcset_id]={"mean":np.mean(norm_dur_list),"std":np.std(norm_dur_list)}
        if (pcset_id==552):
            print("pcset_id 552 norm_dur_list : {}".format(norm_dur_list))
        # values.append(np.mean(norm_dur_list))
        # stds.append(np.std(norm_dur_list))
        # labels.append(str(get_pcset_from_ID(pcset_id)))
        # print("PC : {} mean : {} std : {}".format(pc,mean,std))
    tup = sorted(pcset_durs_infos.items(),key=lambda item: item[1]["mean"],reverse=True)
    print("ordered_pcset_durs_infos : \n\n{}".format(tup[:n]))
    
    values=list(map(lambda x:x[1]["mean"],tup))
    stds = list(map(lambda x:x[1]["std"],tup))
    labels = list(map(lambda x:str(get_pcset_from_ID(x[0])),tup))
    
    # latex fonts
    if latex_fonts :
        labels=list(map(lambda x: x.replace("#","$\sharp$").replace("{","\{").replace("}","\}"),labels))
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')

    plt.figure(figsize=(3, 3))
    # y_pos = np.arange(len(pcset_dur_lists))
    y_pos = np.arange(n)
    # print("y_pos : {}\nvalues : {}".format(y_pos,values[:n]))
    plt.bar(y_pos,values[:n],align='center',width=0.5)
    # half std error bars
    plt.errorbar(y_pos,values[:n],yerr=[np.zeros(n),stds[:n]],fmt = 'none', capsize = 3, ecolor = 'black', elinewidth = 0.5, capthick = 0.5)
    # full std error bars
    # plt.errorbar(y_pos,values[:n],yerr=stds[:n],fmt = 'none', capsize = 3, ecolor = 'black', elinewidth = 0.5, capthick = 0.5)
    plt.xticks(y_pos,labels[:n],rotation=55,fontsize=8)
    plt.ylabel('mean norm duration',fontsize=10)
    plt.xlabel('PCset',fontsize=10)
    plt.ylim(top=1)
    # plt.title(title.replace("ä","\\\"{a}"),fontsize=10)
    plt.tight_layout()  # pour ne pas que les x-labels pivotés sortent de l'image
    plt.savefig('./plots/'+title+'.pdf')
    plt.clf()


def display_PC_histogram(title,pcs_occs,output_dir_path='./plots/',display_gap=0):
    pc_histo = pcs_occs.get_PC_histogram()
    # window_length=pcs_occs._slicing_duration
    labels = []
    values = []
    y_pos = np.arange(len(pc_histo))
    for pc,n in pc_histo.items():
        labels.append(str(pc_names[pc]))
        values.append(n)
        
    # latex fonts
    if latex_fonts :
        labels=list(map(lambda x: x.replace("#","$\sharp$").replace("{","\{").replace("}","\}"),labels))
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
    
    plt.figure(figsize=(3, 3))
    plt.bar(y_pos,values,align='center',width=0.5)
    plt.xticks(y_pos,labels,rotation=0,fontsize=8)
    plt.ylabel('normalized duration',fontsize=10)
    plt.xlabel('pitch class',fontsize=10)
    plt.ylim(top=1)
    if display_gap>0:
        sorted_values = sorted(values,reverse=True)
        sup=sorted_values[display_gap-1]
        inf=sorted_values[display_gap]
        plt.axhline(y=sup, color='r', linestyle=':')
        plt.axhline(y=inf, color='r', linestyle=':')
    plt.title(title.replace("ä","\\\"{a}"),fontsize=10)
    plt.tight_layout()  # pour ne pas que les x-labels pivotés sortent de l'image
    if output_dir_path=="show":
        plt.show()
    else :
        plt.savefig(output_dir_path+title+'.pdf')
        plt.clf()
    

# display mean+std norm_dur for the 12 PC.
def display_corpus_PC_histogram(title,pcs_occs_list,weight_by_slicing_dur=False):
    pc_values_list = {}
    for i in range(12):
        pc_values_list[i]=[]
        
    for pcs_occs in pcs_occs_list:
        pc_histo = pcs_occs.get_PC_histogram()
        print("pc histogram : {}".format(pc_histo))
        for pc, norm_dur in pc_histo.items():
            pc_values_list[pc].append(norm_dur)
            
    labels = []
    values = []
    stds = []
    y_pos = np.arange(len(pc_values_list))
    for pc, norm_dur_list in pc_values_list.items():
        values.append(np.mean(norm_dur_list))
        stds.append(np.std(norm_dur_list))
        labels.append(str(pc_names[pc]))
        # print("PC : {} mean : {} std : {}".format(pc,mean,std))

    # latex fonts
    if latex_fonts :
        labels=list(map(lambda x: x.replace("#","$\sharp$").replace("{","\{").replace("}","\}"),labels))
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')

    plt.figure(figsize=(3, 3))
    plt.bar(y_pos,values,align='center',width=0.5)
    # full std error bars
    # plt.errorbar(y_pos,values,yerr=stds,fmt = 'none', capsize = 3, ecolor = 'black', elinewidth = 0.5, capthick = 0.5)
    # half std error bars
    plt.errorbar(y_pos,values,yerr=[np.zeros(len(stds)),stds],fmt = 'none', capsize = 3, ecolor = 'black', elinewidth = 0.5, capthick = 0.5)
    plt.xticks(y_pos,labels,rotation=0,fontsize=8)
    plt.ylabel('mean norm duration',fontsize=10)
    plt.xlabel('pitch class',fontsize=10)
    plt.ylim(top=1)
    # plt.title(title.replace("ä","\\\"{a}"),fontsize=10)
    plt.tight_layout()  # pour ne pas que les x-labels pivotés sortent de l'image
    plt.savefig('./plots/'+title+'.pdf')
    plt.clf()

# From a corpus, plot the distribution of slicing lengths
def plot_slicing_lengths(corpus_dir,bins=None,min_length=None,max_length=None):
    lengths_distrib = {}
    histo_list = []
    file_count = 0
    distrib_count = 0
    for filename in os.listdir(corpus_dir):
        if filename.endswith(".mid"):
            file_count+=1
            midi_file_name=filename
            midi_file_path=corpus_dir+midi_file_name
            pitch_slicing = PitchSlicing(midi_file_path,None)
            l=len(pitch_slicing.get_slicing())
            if (min_length is not None and l<min_length) or (max_length is not None and l>max_length):
                continue
            distrib_count+=1
            histo_list.append(l)
            if l in lengths_distrib:
                lengths_distrib[l]+=1
            else :
                lengths_distrib[l]=1
    print("lengths_distrib : {}".format(lengths_distrib))
    print("{}/{} files in the range {}-{}".format(distrib_count,file_count,min_length,max_length))
    # plt.bar(list(lengths_distrib.keys()), lengths_distrib.values(), color='g')
    # plt.show()
    plt.hist(histo_list,bins=np.arange(min_length,max_length,10))
    plt.xlabel('slicing lengths')
    plt.ylabel('fragments')
    plt.title('lengths_distrib')
    plt.show()
    
# also plots persistence diagrams
def get_corpus_asc_persistences(pcs_occs_list,pcset_size=None):
    corpus_asc_persistence_list=[]  # list of dict : [{ pcsidset:[..,..,..],
                                    #                   persistences:[(b,d),(b,d),(b,d)],
                                    #                   life_dur_list:[d1,d2,d3],
                                    #                   pers_sum:pers_sum,
                                    #                   pers_count:pers_count,
                                    #                   no_face_asc:no_face_asc
                                    #                  },
                                    #                 {},
                                    #                 {}]
    for pcs_occs in pcs_occs_list:
        ascs_persistence=pcs_occs.get_abstract_simplicial_complexes_persistence(pcset_size=pcset_size)
        for asc_persistence in ascs_persistence:
            asc_id=asc_persistence["pcs_id_set"]    # remember this id is a set ...
            # for pcsid in asc_id:
            #     pcset=get_pcset_from_ID(pcsid)
            #     if (len(pcset)>1):
            #         print("ERROR {}".format(pcset))
            #         exit(code=0)
            already_present = False
            for x in corpus_asc_persistence_list :   # search if this asc is already present in the dict
                if set_equality(x["pcs_id_set"],asc_id):
                    already_present=True
                    x["persistences"].append((asc_persistence["birth_level"],asc_persistence["death_level"]))
                    break
            if not already_present:
                corpus_asc_persistence_list.append({"pcs_id_set":asc_id,"persistences":[(asc_persistence["birth_level"],asc_persistence["death_level"],asc_persistence["file_name"])]})
    # add pers sum and pers count informations (redondant mais plus simple ...)
    for asc in corpus_asc_persistence_list:
        persistences=asc["persistences"]
        pers_sum = sum(map(lambda x:x[0]-x[1],persistences))
        asc["life_dur_list"]=list(map(lambda x:x[0]-x[1],persistences))
        asc["pers_sum"]=pers_sum
        asc["pers_count"]=len(persistences)
        asc["no_face_asc"]=asc_no_face_display(asc["pcs_id_set"])
    
    # display it nicely !
    corpus_asc_persistence_list=sorted(corpus_asc_persistence_list,key = lambda x : x["pers_sum"],reverse=True)
    for asc in corpus_asc_persistence_list:
        if asc["pers_count"]>1:
            print("ASC : {}\n  pers sum = {}\n  pers count = {}".format(asc["no_face_asc"],asc["pers_sum"],asc["pers_count"]))
    
    #plot top n persistence diagrams
    print("\n DISPLAY PERSISTENCE DIAGRAMS\n")
    for i in range(min(15,len(corpus_asc_persistence_list))):
        asc=corpus_asc_persistence_list[i]
        asc_name=str(asc["no_face_asc"])
        birth_list=[]
        death_list=[]
        print(asc_name)
        # {birth,death}=1-{birth,death} to visualize the filtration as a 0->1 process
        for pers in asc["persistences"]:
            birth_list.append(1-pers[0])
            death_list.append(1-pers[1])
            print("birth:{} death:{}".format(1-pers[0],1-pers[1]))
        plt.scatter(birth_list,death_list)
        plt.plot([0,1],[0,1],color='red',linestyle='solid')
        plt.xlabel('birth')
        plt.ylabel('death')
        plt.title(asc_name)
        plt.savefig('./plots/'+asc_name+'.pdf')
        plt.clf()
            
            
    return corpus_asc_persistence_list
                
