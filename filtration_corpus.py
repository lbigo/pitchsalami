import sys,os
import numpy as np

sys.path.insert(0, "./PitchSalami")
from PitchSalami import *

corpus_dir_mozart_CM_test = "../../segments-tonaux/mozart-tonal-midi-C-test/" # 3 pièces en C Majeur
corpus_dir_mozart = "../../segments-tonaux/mozart-tonal-midi-C/" # 316 fragments en C Majeur / C mineur

tonality = "c_major"
#tonality = "c_minor"

_slicing_length_min = 20
_slicing_length_max = 200

def get_pcs_occs_lists_from_corpus(corpus_dir):
    pcs_occs_list=[]
    for filename in os.listdir(corpus_dir):
        if filename.endswith(".mid"):
            if (tonality == "c_major" and filename.endswith("M.mid")) or (tonality=="c_minor" and filename.endswith("m.mid")):
                midi_file_name=filename
                midi_file_path=corpus_dir+midi_file_name
                pitch_slicing = PitchSlicing(midi_file_path,None)
                slicing_length = len(pitch_slicing.get_slicing())
                if slicing_length>=_slicing_length_min and slicing_length<=_slicing_length_max:
                    pcset_occurences=PCSetOccurrences(pitch_slicing=pitch_slicing)
                    # pcset_occurences.get_abstract_simplicial_complexes_persistence()
                    pcs_occs_list.append(pcset_occurences)
    return pcs_occs_list

def plot_distribution(title,values):
    plt.hist(values,bins=np.arange(0,1,0.05))
    plt.xlabel('persistence')
    plt.ylabel('fragments')
    plt.title(title)
    # plt.show()
    plt.savefig(title+'.pdf')
    plt.clf()

# probablement inutile - visiblement peu de corélation entre persistence des sets et longueur de l'extrait
def plot_pers_length(title,persistences,lengths):
    plt.scatter(lengths,persistences)
    plt.xlabel('slicing length')
    plt.ylabel('persistence')
    plt.title(title)
    # plt.show()
    plt.savefig("pers-length_"+title+'.pdf')
    plt.clf()
    


_pcs_occs_list=get_pcs_occs_lists_from_corpus(corpus_dir_mozart)
# _pcs_occs_list=get_pcs_occs_lists_from_corpus(corpus_dir_mozart_CM_test)

####### GET PERSISTENT ASCS AND PLOT PERSISTENCE DIAGRAMS ##########
# corpus_asc_persistences=get_corpus_asc_persistences(pcs_occs_list,pcset_size=1)
# print("corpus_asc_persistences : \n{}".format(corpus_asc_persistences))
# print("nombre de fragments entre {} et {} : {}".format(_slicing_length_min,_slicing_length_max,len(_pcs_occs_list)))
####################################################################


############### DISPLAY SEQ LENGTHS IN CORPUS ######################
# plot_slicing_lengths(corpus_dir,min_length=_slicing_length_min,max_length=_slicing_length_max)
####################################################################


############### DISPLAY PC DISTRIB IN CORPUS #######################
# display_corpus_PC_histogram(tonality+"_PC_nb_slices=["+str(_slicing_length_min)+"-"+str(_slicing_length_max)+"]",pcs_occs_list)
####################################################################


############### DISPLAY MOST FREQUENT PCSS IN CORPUS #######################
# chord_size = 3
# topN = 10
# plot_name = tonality+"_"+str(chord_size)+"-pcsets-top"+str(topN)
# display_corpus_most_frequent_pcss(plot_name,_pcs_occs_list,n=topN,s=chord_size)
############################################################################

###### GET GAPS OF EVERY SET of n-PCS IN CORPUS ####################
n=1
weight_gaps_by_slice_duration = False
# get every sets
sets = []
for pcs_occs in _pcs_occs_list:
    for persistent_set in pcs_occs.get_persistent_sets(n):
        if persistent_set[0] not in sets : sets.append(persistent_set[0])

# sets_gaps: [(set_of_IDs,[dur_list],[gap_list]) , (set_of_IDs,[dur_list],[gap_list]) , ...]
sets_gaps = []
for s in sets : # initialize list with empty dur/gap lists
    sets_gaps.append((s,[],[]))
for pcs_occs in _pcs_occs_list:
    for s in sets_gaps:
        present_in_occs = False
        persistence = 0
        for pers_set in pcs_occs.get_persistent_sets(n):
            if pers_set[0]==s[0]:
                assert present_in_occs==False , "erreur : doublon quelquepart"
                present_in_occs=True
                persistence=pers_set[1]
        s[1].append(pcs_occs._slicing_duration)
        s[2].append(persistence)

if weight_gaps_by_slice_duration:
    sets_gaps.sort(key=lambda x:np.mean([a*b for a,b in zip(x[1],x[2])]),reverse=True) # sort by weighting gaps by slicing durations
else :
    sets_gaps.sort(key=lambda x:np.mean(x[2]),reverse=True) # sort without taking into account slicing durations

for i in range(10):
    s = sets_gaps[i]
    print(s[1])
    exit(code=0)
    non_zero_persistences_proportion = np.count_nonzero(s[2])/len(s[2])
    if weight_gaps_by_slice_duration:
        weighted_persistences = [a*b for a,b in zip(s[1],s[2])]
        print("mean pers={:.2f} non-zero={:.0%}: {}".format(np.mean(weighted_persistences),non_zero_persistences_proportion,get_set_str_from_IDs(s[0])))
        plot_distribution(get_set_str_from_IDs(s[0]),weighted_persistences)
    else :
        print("mean pers={:.2f} non-zero={:.0%}: {}".format(np.mean(s[2]),non_zero_persistences_proportion,get_set_str_from_IDs(s[0])))
        plot_distribution(get_set_str_from_IDs(s[0]),s[2])
        # plot_pers_length(get_set_str_from_IDs(s[0]),s[2],s[1])
    

####################################################################
