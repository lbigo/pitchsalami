Representation of a midi file as a sequence of pitch slices.

PitchSalami requires `mido` to work :

```
pip3 install mido
```


To slice a midi file, just instanciate an object `PitchSlicing` by providing the path of the midi file :

```
pitch_salami = PitchSlicing('./my_midi_file.mid')
```

The set of pitch slices can then be accessed as a python dictionary.
* The keys of the elements are offsets (in midi ticks) of the slices.
* The values are a dictionaries including :
  * the set of pitches sounding during the slice (key : `pitch_set`)
  * the duration of the slice in midi ticks (key : `duration`)
  
The method `display()` can be used to display the slices :

```
pitch_salami.display()
```

You should get a display similar to this :

```
…
key  5430  :  {'pitch_set': [71, 67, 64, 48], 'duration': 30}
key  5460  :  {'pitch_set': [72, 64, 64, 48], 'duration': 60}
key  5520  :  {'pitch_set': [69, 66, 62, 50], 'duration': 60}
…
```

To display the pitch-class distribution, just set the following variable on (l.269 in filtration.py). Histograms are saved in the directory `./plots`

```
save_pc_histo = True
```

To display the 10 (or n) most prevalent pc-sets, just uncomment line 288 :

```
display_most_frequent_pcss(piece_names[midi_file_name],pcset_occurences,10)
```

To display the filtration steps based on pc-set cumulative duration, uncomment line 298 :

```
display_filtration_comparison(pcs_occs_list,title_list)
```
