from __future__ import print_function
import sys
import os
import librosa
import numpy as np
import collections

sys.path.insert(0, "./PitchSalami")
from PitchSalami import *
sys.path.insert(0, "./filtration")
from filtration import *

np.set_printoptions(threshold=np.inf)   # pour afficher les np matrices en entier dans le terminal

pc_chroma_treshold = 0.9    # seuil à partir du quel on considère que la pitch class occure

filename = librosa.util.example_audio_file()
filename_tab = '/Users/lbigo/fichiers_audio/Muse - Take A Bow.mp3'

# ex : [0,4,7] -> [True,False,False,False,True,False,False,True,False,False,False,False]
def pcset_to_bool_chroma(pcset):
    bool_chroma = []
    for i in range(12):
        if i in pcset :
            bool_chroma.append(True)
        else :
            bool_chroma.append(False)
    return bool_chroma
        
# ex : [True,False,False,False,True,False,False,True,False,False,False,False] -> [0,4,7]
def bool_chroma_to_pcset(chroma):
    pcset=[]
    for i in range(len(chroma)):
        if chroma[i] :
            pcset.append(i)
            
    return pcset

# return ordered dict of the form {key:offset,value:{PCset:[..],duration:N}}
# gives the same duration to each slice
def pcset_list_to_pc_slice_dict(pcs_list,slice_length):
    pc_slice_dict = collections.OrderedDict()
    offset = 0
    for i in range(len(pcs_list)):
        pcset = pcs_list[i]
        pc_slice_dict[offset]={"pc_set":pcset.copy(),"duration":slice_length}
        offset+=slice_length
    return pc_slice_dict

y,sr = librosa.load(filename_tab)
print("nombre de samples : {}".format(len(y)))
print("Fe : {}".format(sr))
# Set the hop length; at 22050 Hz, 512 samples ~= 23ms
#hop_length = 512

# Separate harmonics and percussives into two waveforms
y_harmonic, y_percussive = librosa.effects.hpss(y)


# Compute chroma features from the harmonic signal
chromagram = librosa.feature.chroma_cqt(y=y_harmonic,sr=sr) # par défault, le chroma est calculé sur des segments contigus de 512 samples ~23ms pour 22050
print('chromagram :\n',chromagram)

print('Shape={}'.format(chromagram.shape))

#print(chromagram[:5,])
print(chromagram[:,:5])

chromagram_T = chromagram.transpose()
treshold_chromagram_T = chromagram_T>pc_chroma_treshold
print("chromagram_T:{}".format(treshold_chromagram_T[:,:50]))

pcset_sequence = list(map(lambda x: bool_chroma_to_pcset(x),treshold_chromagram_T))
print(pcset_sequence)
print("length : ",len(pcset_sequence))
pc_slice_dict=pcset_list_to_pc_slice_dict(pcset_sequence,512)

pcset_occurences=PCSetOccurrences(audio_pc_slice_dict=pc_slice_dict)

display_PC_histogram("Take A Bow",pcset_occurences)
