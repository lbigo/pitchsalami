import sys
import os
import librosa
import numpy as np

sys.path.insert(0, "./PitchSalami")
from PitchSalami import *


midi_dir_path = "/Users/lbigo/fichiersMIDI/Bach/Bach-371-Chorals-sources-ly_midi-6/sources-midi/"
audio_dir_path = "/Users/lbigo/fichiersMIDI/Bach/Bach-371-Chorals-sources-ly_midi-6/sources-fluidsynth/"

pc_chroma_treshold = 0.7

# ex : [0,4,7] -> [True,False,False,False,True,False,False,True,False,False,False,False]
def pcset_to_bool_chroma(pcset):
    bool_chroma = []
    for i in range(12):
        if i in pcset :
            bool_chroma.append(True)
        else :
            bool_chroma.append(False)
    return bool_chroma

# ex : [True,False,False,False,True,False,False,True,False,False,False,False] -> [0,4,7]
def bool_chroma_to_pcset(chroma):
    pcset=[]
    for i in range(len(chroma)):
        if chroma[i] :
            pcset.append(i)
            
    return pcset

def eval_chroma_vector(audio_bool_vec, midi_bool_vec):
    TP=0
    TN=0
    FP=0
    FN=0
    for i in range(len(audio_bool_vec)):
        if audio_bool_vec[i] and midi_bool_vec[i]:
            TP+=1
        if audio_bool_vec[i] and not midi_bool_vec[i]:
            FP+=1
        if not audio_bool_vec[i] and midi_bool_vec[i]:
            FN+=1
        if not audio_bool_vec[i] and not midi_bool_vec[i]:
            TN+=1
    return TP,TN,FP,FN
    
eval_dict = {}


number_of_matching_pc = 0
number_of_eval_pc = 0
total_FP = 0
total_FN = 0

#for treshold in range(0.99,1,0.001)

for midi_file_name in os.listdir(midi_dir_path):
    if midi_file_name.endswith(".midi"):
        piece_name = midi_file_name.replace(".midi","")
        wav_file_name = piece_name+".wav"
        print("piece : ",piece_name)
        audio_file_path = audio_dir_path+wav_file_name
        if not os.path.isfile(audio_file_path):
            print("no wave file found for midi file {} - {}".format(midi_file_name,audio_file_path))
            exit()
        else :
            pitch_slicing = PitchSlicing(midi_dir_path+midi_file_name,None)
            midi_sec_duration = pitch_slicing._mid.length
            midi_tick_duration = pitch_slicing.get_slicing_duration()
            #pitch_slicing.display()
            y,sr = librosa.load(audio_file_path)
            # print("nombre de samples : {}".format(len(y)))
            # print("Fe : {}".format(sr))
            audio_sec_duration = librosa.get_duration(y=y,sr=sr)
            #trunc_1s_audio_sec_duration = audio_sec_duration-1
            trunc_1s_audio_sec_duration = audio_sec_duration    # test.midi
            #print("audio duration : ",audio_duration)
            if abs(trunc_1s_audio_sec_duration-midi_sec_duration) > 0.05:
                print("difference between audio {} and midi {} ".format(trunc_1s_audio_sec_duration,midi_sec_duration))
                exit()
            #y_trunc = y[:-22050]    # remove the last 1 second
            y_trunc = y    # test.midi
            audio_sec_duration_y_trunc = librosa.get_duration(y=y_trunc,sr=sr)
            if (abs(audio_sec_duration_y_trunc-trunc_1s_audio_sec_duration)>0.005):
                print("ERROR {} {}".format(audio_sec_duration_y_trunc,trunc_1s_audio_sec_duration))
                exit()
            y_harmonic, y_percussive = librosa.effects.hpss(y_trunc)
            chromagram = librosa.feature.chroma_cqt(y=y_harmonic,sr=sr) # par défault, le chroma est calculé sur des segments contigus de 512 samples ~23ms pour 22050
            chromagram_T = chromagram.transpose()
            treshold_chromagram_T = chromagram_T>pc_chroma_treshold
            midi_pc_slicing = pitch_slicing.get_pc_slicing()
            for audio_slice_index in range(len(treshold_chromagram_T)):
                print("audio_slice_index:",audio_slice_index)
                audio_bool_chroma = treshold_chromagram_T[audio_slice_index]
                date_sec = audio_slice_index*(512/22050)
                one_midi_tick_duration = midi_sec_duration/midi_tick_duration
                date_midi_tick = round(date_sec/one_midi_tick_duration)
                covering_slice_key = pitch_slicing.get_covering_slice_key(date_midi_tick)
                covering_pc_slice = midi_pc_slicing[covering_slice_key]
                # if (audio_slice_index%100) == 0:
                #     print("covering_pc_slice:",covering_pc_slice," ",covering_slice_key)
                midi_bool_chroma = np.array(pcset_to_bool_chroma(covering_pc_slice['pc_set']))
                print("date : {} tick : {}".format(date_sec,date_midi_tick))
                print("chroma vector in midi : {} - {}".format(midi_bool_chroma,bool_chroma_to_pcset(midi_bool_chroma)))
                print("chroma vector in audi : {} - {}".format(audio_bool_chroma,bool_chroma_to_pcset(audio_bool_chroma)))
                print(np.sum(midi_bool_chroma==audio_bool_chroma))
                print("\n")
                number_of_matching_pc+=np.sum(midi_bool_chroma==audio_bool_chroma)
                TP,TN,FP,FN = eval_chroma_vector(audio_bool_chroma, midi_bool_chroma)
                number_of_eval_pc+=12
                total_FP+=FP
                total_FN+=FN

#print("treshold {} number of matching pc : {}/{} = {}%".format(pc_chroma_treshold,number_of_matching_pc,number_of_eval_pc,100*number_of_matching_pc/number_of_eval_pc))
print("treshold {} FP = {} FN = {} - number of matching pc : {}/{} = {}%".format(pc_chroma_treshold,total_FP,total_FN,number_of_matching_pc,number_of_eval_pc,100*number_of_matching_pc/number_of_eval_pc))
#eval_dict[pc_chroma_treshold]=number_of_matching_pc/number_of_eval_pc
